library vertical_load_more;

import 'package:flutter/material.dart';

/// this class user for controlling data of Load More
/// Normally we use on hasMore to set Load More state
class VerticalLoadMoreController {
  bool hasMore = true;
  bool showMoreIndicator = false;
  bool isLoadDataSuccess = true;

  /// to reset loading state
  void resetLoadingState() {
    hasMore = true;
    showMoreIndicator = false;
    isLoadDataSuccess = true;
  }
}

class VerticalLoadMore extends StatefulWidget {
  final Widget child;
  final Function onLoadMore;
  final Function onRefresh;
  final Color color;
  final Widget? loadMoreWidget;
  final VerticalLoadMoreController controller;

  const VerticalLoadMore({
    Key? key,
    required this.child,
    required this.onLoadMore,
    required this.onRefresh(),
    this.color = Colors.green,
    this.loadMoreWidget,
    required this.controller,
  }) : super(key: key);

  @override
  State<VerticalLoadMore> createState() => _EasyLoadingMoreState();
}

class _EasyLoadingMoreState extends State<VerticalLoadMore> {
  /// init scroll controller to controll scrolling state
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    /// listener for scrolling state for scroll widget
    scrollController.addListener(() {
      /// need to check if it should show load more or not
      /// 30 offset to before it finish scrolling
      if ((scrollController.offset + 30) >=
              scrollController.position.maxScrollExtent &&
          widget.controller.hasMore &&
          widget.controller.isLoadDataSuccess) {
        widget.controller.showMoreIndicator = true;
        widget.controller.isLoadDataSuccess = false;
        setState(() {});

        /// hide loading widget when it loaded the data
        /// we need to set a bit duration before it hides
        /// this prevent blink screen
        widget.onLoadMore().then((value) {
          Future.delayed(const Duration(milliseconds: 300), () {
            widget.controller.showMoreIndicator = false;
            widget.controller.isLoadDataSuccess = true;
            setState(() {});
          });
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      color: widget.color,
      onRefresh: () => widget.onRefresh(),
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          PrimaryScrollController(
            controller: scrollController,
            child: widget.child,
          ),
          if (widget.controller.showMoreIndicator)
            Positioned(
              bottom: (10 + MediaQuery.of(context).padding.bottom),
              right: 0,
              left: 0,
              child: Align(
                alignment: Alignment.center,
                child: (widget.loadMoreWidget == null)
                    ? Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          boxShadow: const [
                            BoxShadow(blurRadius: 2, color: Colors.grey),
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                        ),
                        padding: const EdgeInsets.all(10.0),
                        child: CircularProgressIndicator(
                          color: widget.color,
                          strokeWidth: 3,
                        ),
                      )
                    : widget.loadMoreWidget,
              ),
            )
        ],
      ),
    );
  }
}
