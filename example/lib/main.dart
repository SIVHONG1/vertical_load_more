import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vertical_load_more/vertical_load_more.dart';

void main() {
  runApp(const MyApp());
}

/// example easy load more
class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late List<Map<String, dynamic>> data;

  Random randomColor = Random();
  VerticalLoadMoreController verticalLoadMoreController =
      VerticalLoadMoreController();
  @override
  void initState() {
    verticalLoadMoreController.hasMore = true;
    setData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vertical Load More Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: SafeArea(
          child: VerticalLoadMore(
            child: ListView.builder(
              itemBuilder: (_, index) {
                return Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: data[index]["color"],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    data[index]["value"].toString(),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                );
              },
              itemCount: data.length,
            ),
            onLoadMore: onMoreData,
            onRefresh: onRefresh,
            controller: verticalLoadMoreController,
          ),
        ),
      ),
    );
  }

  Future<void> onMoreData() async {
    await Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        verticalLoadMoreController.hasMore = true;
        data.addAll(
          List.generate(
            10,
            (index) => {
              "value": "ស្រុករំដួល",
              "color": Color.fromRGBO(
                randomColor.nextInt(255),
                randomColor.nextInt(255),
                randomColor.nextInt(255),
                0.4,
              ),
            },
          ),
        );
      });
    });
  }

  Future<void> onRefresh() async {
    await Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        verticalLoadMoreController.hasMore = true;
        data = List.generate(
          10,
          (index) => {
            "value": "ទីក្រុងគុជ",
            "color": Color.fromRGBO(
              randomColor.nextInt(255),
              randomColor.nextInt(255),
              randomColor.nextInt(255),
              0.4,
            ),
          },
        );
      });
    });
  }

  void setData() {
    data = List.generate(
      10,
      (index) => {
        "value": "ខ្មែរស្រឡាញ់ខ្មែរKSK",
        "color": Color.fromRGBO(
          randomColor.nextInt(255),
          randomColor.nextInt(255),
          randomColor.nextInt(255),
          0.4,
        ),
      },
    );
  }
}
