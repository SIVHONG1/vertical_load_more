
## ផេកខេច ទុកទិន្នន័យជាបន្តបន្ទាប់ និង ការទាញទិន្នន័យឡើងវិញ Load More & Pull To Refresh Package

[![License](https://img.shields.io/badge/license-MIT-green.svg)](/LICENSE)
[![Platform Flutter](https://img.shields.io/badge/platform-Flutter-blue.svg)](https://flutter.dev)

[![alt](https://icons.iconarchive.com/icons/custom-icon-design/mono-business-2/32/coffee-icon.png)ទិញកាហ្វេអោយខ្ញុំ? Buy me a cup of coffee?](https://www.buymeacoffee.com/sivhong007)


English language below


## ភាសាខ្មែរ 

### ងាយស្រួលប្រើ និង មានទំហំតូច សម្រាប់ការផ្ទុកទិន្នន័យជាបន្តបន្ទាប់ និង ការទាញទិន្នន័យឡើងវិញ

ផេកខេច ដែលជួយលោកអ្នកក្នុងការផ្ទុកទិន្នន័យជាបន្តបន្ទាប់ និង ការទាញទិន្នន័យឡើងវិញ។

*ប្រសិនបើអ្នកត្រូវការ ការផ្ទុកទិន្នន័យជាបន្តបន្ទាប់ ឬ ការទាញទិន្នន័យឡើងវិញ នោះលោកអ្នករកត្រូវកន្លែងហើយ*

អ្នកអាចប្រើវាបានដោយ៖
```
 dart pub add vertical_load_more
```

ឬ

```
 flutter pub add vertical_load_more
```





## កូដគំរូ


ដើម្បីប្រើផេកខេចនេះ អ្នកត្រូវតែផ្តល់មេសូដសម្រាប់ ការផ្ទុកទិន្នន័យជាបន្តបន្ទាប់, ការទាញទិន្នន័យឡើងវិញ និងខុនត្រូឡឺជារបស់វា។ លោកអ្នកអាចគ្រប់គ្រង ការផ្ទុកទិន្នន័យជាបន្តបន្ទាប់ តាមតម្លៃ hasMore ក្នុងខុនត្រូឡឺ VerticalLoadMoreController។ 

```dart
  VerticalLoadMore(
    onRefresh: () async {
      ....
    },
    onLoadMore: () async {
      ....
    },
    controller: verticalLoadMoreController,
    child: ListView(),
  );
```
---


Khmer language above
## English 

### Vertical to use and light-weight package for Loading Data and Pull To Refresh data.

A package that help you to load data infinitely.\
It also handles on pulling to refresh the data.

**If you need a any kind of Load More or Pull To Refresh package , you are in the right place**

You can use it by:
```
 dart pub add vertical_loading_more
```

Or

```
 flutter pub add vertical_loading_more
```

## Sample Code


In order to use this package, you must provide methods for loading the data, refresh data and its controller. You can set hasMore property in VerticalLoadMoreController to controller the Load More.

```dart
  VerticalLoadMore(
    onRefresh: () async {
      ....
    },
    onLoadMore: () async {
      ....
    },
    controller: verticalLoadMoreController,
    child: ListView(),
  );
```

![alt text](https://gitlab.com/SIVHONG1/vertical_load_more/-/raw/main/demo.gif?inline=false)


MIT License

Copyright (c) 2022 SIVHONG DEAB

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#### pagination scroll list vertical refresh loading pulling pull to refresh loadmore infinite scrolling load more & lazy loading